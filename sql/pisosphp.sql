-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-07-2017 a las 17:36:23
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pisosphp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `idCom` int(11) NOT NULL,
  `nombreCom` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `textoCom` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fechaCom` datetime NOT NULL,
  `idUsu` int(11) NOT NULL,
  `idPiso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`idCom`, `nombreCom`, `textoCom`, `fechaCom`, `idUsu`, `idPiso`) VALUES
(1, 'comentario 12', 'comentario 12', '2017-07-03 00:00:00', 1, 2),
(2, 'comentario 2', 'comentario 2', '2017-07-03 00:00:00', 1, 2),
(3, 'comentario 3', 'comentario 3', '2017-07-03 00:00:00', 1, 2),
(4, 'comentario 4', 'comentario 4', '2017-07-03 00:00:00', 1, 3),
(6, 'Comentario 10', 'Texto de comentario 10', '2017-07-03 16:56:45', 2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `idNot` int(11) NOT NULL,
  `tituloNot` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `contenidoNot` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `autorNot` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `fechaNot` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`idNot`, `tituloNot`, `contenidoNot`, `autorNot`, `fechaNot`) VALUES
(1, 'Las hipotecas sobre viviendas suben un 14% en 2016', 'Según datos del Instituto Nacional de Estadística (INE), la concesión de hipotecas sobre viviendas ha subido un 14% en el conjunto de 2016 frente al año anterior, alcanzando las 281.328 firmas.', 'PepiloListo', '2017-06-25 16:28:00'),
(2, 'Cambio de tendencia en el mercado hipotecario', 'Durante las últimas seis semanas las novedades experimentadas por los bancos se han centrado en encarecer los tipos de sus hipotecas, afectando a 20 productos en total.', 'erBarcenAs', '2017-06-23 00:00:00'),
(3, 'El empleo y la escasez de oferta marcan el precio del alquiler', 'La recuperación del empleo es uno de los factores que determinan el auge del precio del alquiler en España. El cambio de tendencia es consecuencia de la mayor necesidad de movilidad laboral.', 'erBarcenAs', '2017-06-23 00:00:00'),
(4, 'El empleo y la escasez de oferta marcan el precio del alquiler', 'La recuperación del empleo es uno de los factores que determinan el auge del precio del alquiler en España. El cambio de tendencia es consecuencia de la mayor necesidad de movilidad laboral.', 'erBarcenAsII', '2017-06-23 00:00:00'),
(5, 'El TS pide aclarar los intereses de demora abusivos en los mercados bursatiles', 'El Tribunal Supremo tiene la intención de que la justicia europea determine si los intereses de demora de un préstamo deben considerarse desproporcionados para el cliente si estos son superiores al 2%.', 'leMontoro', '2017-06-26 18:06:32'),
(6, '', '', '', '2017-07-03 16:00:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pisos`
--

CREATE TABLE `pisos` (
  `idPiso` int(11) NOT NULL,
  `direccionPiso` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `caracteristicasPiso` longtext COLLATE utf8_spanish_ci NOT NULL,
  `imagenPiso` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `precioPiso` float NOT NULL,
  `ciudadPiso` tinytext COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pisos`
--

INSERT INTO `pisos` (`idPiso`, `direccionPiso`, `caracteristicasPiso`, `imagenPiso`, `precioPiso`, `ciudadPiso`) VALUES
(2, 'Plaza Roma</body>', ' Los pisos industriales son estructuras de concreto con características muy específicas para garantizar un comportamiento que permita desarrollar sobre éstas diferentes procesos en condiciones de servicio.<br><br>\r\n \r\nUn piso industrial es una superfic', '', 220000, 'Zaragoza'),
(3, 'Escoriaza y Fabro 16', ' A continuación le presentamos a Concretos Poliméricos de México S.A. de C.V., proveedor de pisos industriales:<br><br>\r\n \r\nConcretos Poliméricos de México S.A. de C.V., es el fabricante de Concreto Polimérico prefabricado para el sector de la construcción.<br><br>\r\n\r\n</body>\r\n\r\n \r\nInició con paneles para fachadas y actualmente su línea de fabricación de prefabricados es amplia y de acuerdo a los proyectos de sus clientes, con el compromiso de mejorar continuamente sus procesos, productos y servicios, lo que los ha llevado a ser una empresa versátil.<br><br>\r\n \r\nConozca el Perfil, Productos, Dirección y Teléfono de Concretos Poliméricos de México S.A. de C.V.<br><br>\r\n \r\nO bien, haga contacto directo con Concretos Poliméricos de México S.A. de C.V., para solicitar mayor información sobre sus pisos industriales.<br><br>', '', 190000, 'zaragoza'),
(4, 'Su casa de ensueño 19', 'Los pisos flotantes son uno de los revestimientos preferidos por quienes están construyendo su hogar familiar. Son cálidos, fáciles de mantener y aportan una elegancia única a cualquier ambiente. Aunque su costo es algo elevado, bien vale la pena cuando tomas en consideración su durabilidad, siempre que esté colocado por manos experimentadas en ello.<br><br> \r\n\r\nSe trata de un revestimiento de suelos en el que se colocan maderas encoladas entre sí, y por sobre diversas superficies secas, firmes y planas. Estos entablonados pueden colocarse sobre maderas existentes, carpetas y también sobre embaldosados, mosaicos y pisos cerámicos, reduciendo su costo de restauración ante daños y permitiendo un piso más cálido y llamativo.', 'piso5.jpg', 325000, 'Zaragoza'),
(5, 'aaa', 'aaa', 'nodisponible.jpg', 0, 'aaa'),
(6, 'bbbb', 'bbbb', 'nodisponible.jpg', 0, 'bbbbb'),
(7, 'cccc', 'cccc', 'nodisponible.jpg', 0, 'cccc'),
(8, 'dddd', 'ddd', 'nodisponible.jpg', 0, 'dddd'),
(9, 'Piso en las Ramblas', 'Pedazo de piso', '1500301892fondo1024x768_new.jpg', 400000, 'Barcelona');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsu` int(11) NOT NULL,
  `nombreUsu` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `claveUsu` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `correoUsu` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `tipoUsu` tinytext COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsu`, `nombreUsu`, `claveUsu`, `correoUsu`, `tipoUsu`) VALUES
(1, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 'admin@gmail.com', 'administrador'),
(2, 'invitado', '81dc9bdb52d04dc20036dbd8313ed055', 'invitado@gmail.com', 'normal');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `votos`
--

CREATE TABLE `votos` (
  `idVoto` int(11) NOT NULL,
  `idPiso` int(11) NOT NULL,
  `idUsu` int(11) NOT NULL,
  `puntuacionVoto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `votos`
--

INSERT INTO `votos` (`idVoto`, `idPiso`, `idUsu`, `puntuacionVoto`) VALUES
(1, 3, 1, 2),
(2, 2, 1, 4),
(3, 3, 1, 3),
(4, 3, 1, 4),
(5, 3, 1, 1),
(6, 3, 1, 1),
(7, 3, 1, 3),
(8, 2, 1, 5),
(9, 2, 1, 5),
(10, 2, 1, 4),
(11, 2, 1, 1),
(12, 2, 1, 1),
(13, 2, 1, 1),
(14, 2, 1, 1),
(15, 2, 1, 1),
(16, 2, 1, 1),
(17, 2, 1, 1),
(18, 2, 1, 1),
(19, 2, 1, 1),
(20, 2, 1, 1),
(21, 2, 1, 1),
(22, 2, 1, 4),
(23, 2, 1, 4),
(24, 2, 1, 5),
(25, 2, 1, 5),
(26, 2, 1, 5),
(27, 2, 1, 5),
(28, 2, 1, 5),
(29, 2, 1, 5),
(30, 2, 1, 5),
(31, 2, 1, 5),
(32, 2, 1, 5),
(33, 2, 1, 5),
(34, 2, 1, 5),
(35, 2, 1, 5),
(36, 2, 1, 5),
(37, 2, 1, 5),
(38, 2, 1, 5),
(39, 2, 1, 5),
(40, 2, 1, 5),
(41, 2, 1, 5),
(42, 3, 1, 3),
(43, 3, 1, 5),
(44, 3, 1, 5),
(45, 3, 1, 5),
(46, 4, 1, 4),
(47, 4, 1, 5),
(48, 4, 1, 2),
(49, 4, 1, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`idCom`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`idNot`);

--
-- Indices de la tabla `pisos`
--
ALTER TABLE `pisos`
  ADD PRIMARY KEY (`idPiso`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsu`);

--
-- Indices de la tabla `votos`
--
ALTER TABLE `votos`
  ADD PRIMARY KEY (`idVoto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `idCom` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `idNot` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `pisos`
--
ALTER TABLE `pisos`
  MODIFY `idPiso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `votos`
--
ALTER TABLE `votos`
  MODIFY `idVoto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
