<?php
//Fichero    /extensiones.php

$function = new Twig_SimpleFunction('menu', function(){
	//Necesito cierta informacion
	//El controlador actual, el vector de titulos de menu
	// el vector de controladores de menu
	global $contr;
	global $menuEnlaces;
	global $menuTitulos;

	//a partir de ahora, contruyo el menu que devolvera la funcion
	$r='<ul class="nav nav-tabs">';
	for($i=0;$i<count($menuEnlaces);$i++){
		if($menuEnlaces[$i]==$contr){
			$cl='active';
		}else{
			$cl='';
		}
		$r.='<li class="'.$cl.'"><a href="index.php?contr='.$menuEnlaces[$i].'">'.$menuTitulos[$i].'</a></li>';
	}
	$r.='</ul>';
	return $r;
});
$twig->addFunction($function);

//////////////////////////////////////////////////////////////////////////////////
// Se llamara asi: {{ paginacion(numpag, numPaginas, numregistrosporpagina)|raw }}
$function = new Twig_SimpleFunction('paginacion', function($numpag, $numPaginas, $numregistrosporpagina){

	global $contr;

	$r='<ul class="pagination">';

	if($numpag > 0){
  		$r.='<li><a href="index.php?contr='.$contr.'&accion=listado&numpag='.($numpag-1).'">&laquo;</a></li>';
	}else{
  		$r.='<li><a href="#">&laquo;</a></li>';
	}

	for($i=1;$i<=$numPaginas;$i++){
		if ($i == $numpag+1){
 			$r.='<li class="active"><a href="index.php?contr='.$contr.'&accion=listado&numpag='.($i-1).'">'.$i.'</a></li>';
		}else{
			$r.='<li><a href="index.php?contr='.$contr.'&accion=listado&numpag='.($i-1).'">'.$i.'</a></li>';
		}
	}
	
	if ($numpag < $numPaginas-1){	
  		$r.='<li><a href="index.php?contr='.$contr.'&accion=listado&numpag='.($numpag+1).'">&raquo;</a></li>';
  	}else{
  		$r.='<li><a href="#">&raquo;</a></li>';
  	}

	$r.='</ul>';

	return $r;
});
$twig->addFunction($function);

?>
