<?php  
//Fichero  controllers/pisosController.php
require('models/class.noticiasModel.php');

$noticias=new NoticiasModel();

//Evaluamos la accion a realizar
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='listado';
}

switch($accion){
	case 'detalle':
		$id=$_GET['id'];
		$elementos=$noticias->detalle($id);
		echo $twig->render('noticiasDetalle.html',array('elemento'=>$elementos));
		break;

	case 'insertar':
		echo $twig->render('noticiasInsertar.html',array());
		break;	
	
	case 'insercion':

		$tituloNot=$_POST['tituloNot'];
		$contenidoNot=$_POST['contenidoNot'];
		$autorNot=$_POST['autorNot'];
		$fechaNot=date('Y-m-d H:i:s');

		$consulta=$noticias->insertar($tituloNot, $contenidoNot, $autorNot, $fechaNot);

		//Consulta ESTRICTA!!!!!!
		if($consulta===true){
			header('Location:index.php?contr=noticias.php&accion=listado');
		}else{
			$error=$consulta;
			echo $twig->render('noticiasError.html', array('error'=>$error));
		}

		break;

	case 'borrar':
		$id=$_GET['id'];

		$consulta=$noticias->borrar($id);

		//Consulta ESTRICTA!!!!!!
		if($consulta===true){
			header('Location:index.php?contr=noticias.php&accion=listado');
		}else{
			$error=$consulta;
			echo $twig->render('noticiasError.html', array('error'=>$error));
		}
		break;

	case 'modificar':
		$id=$_GET['id'];
		$e=$noticias->detalle($id);
		//TODO: 
		echo $twig->render('noticiasModificar.html', array('e'=>$e));
		break;

	case 'modificacion':

		$idNot=$_POST['idNot'];
		$tituloNot=$_POST['tituloNot'];
		$contenidoNot=$_POST['contenidoNot'];
		$autorNot=$_POST['autorNot'];
		$fechaNot=date('Y-m-d H:i:s');

		$consulta=$noticias-> modificar( $idNot, $tituloNot, $contenidoNot, $autorNot, $fechaNot);

		//Consulta ESTRICTA!!!!!!
		if($consulta===true){
			header('Location:index.php?contr=noticias.php&accion=listado');
		}else{
			$error=$consulta;
			echo $twig->render('noticiasError.html', array('error'=>$error));
		}

		break;

	case 'listado':
	default:
		//Estraigo el listado de pisos
		
		if(isset($_GET['numpag'])){
			$numpag=$_GET['numpag'];
		}else{
			$numpag=0;
		}

		$numregporpagina=2;

		$numPaginas=$noticias->numPaginas($numregporpagina);
		$elementos=$noticias->listado($numregporpagina,$numpag);
		echo $twig->render('noticiasListado.html', array('numPaginas'=>$numPaginas, 'numregporpagina'=>$numregporpagina, 'numpag'=>$numpag, 'elementos'=>$elementos));
		break;


}
?>