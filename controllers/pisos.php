<?php  
//Fichero  controllers/pisosController.php
require('models/class.pisosModel.php');
require('models/class.rssModel.php');
$pisos=new PisosModel();

//Evaluamos la accion a realizar
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='listado';
}

switch($accion){
	case 'detalle':
		$id=$_GET['id'];
		$e=$pisos->detalle($id);
		echo $twig->render('pisosDetalle.html', array('elemento'=>$e));
		break;

	case 'insertar':
		echo $twig->render('pisosInsertar.html', array());
		break;	
	
	case 'insercion':

		$direccionPiso=$_POST['direccionPiso'];
		$caracteristicasPiso=$_POST['caracteristicasPiso'];
		$precioPiso=$_POST['precioPiso'];
		$ciudadPiso=$_POST['ciudadPiso'];

		if(is_uploaded_file($_FILES['imagenPiso']['tmp_name'])){
			$imagenPiso=time().$_FILES['imagenPiso']['name'];
			move_uploaded_file($_FILES['imagenPiso']['tmp_name'], 'imagenes/'.$imagenPiso);
		}else{
			$imagenPiso='nodisponible.jpg';
		}

		$consulta=$pisos->insertar($direccionPiso, $caracteristicasPiso, $precioPiso, $ciudadPiso, $imagenPiso);

		//Consulta ESTRICTA!!!!!!
		if($consulta===true){
			header('Location:index.php?contr=pisos.php&accion=listado');
		}else{
			$error=$consulta;
			echo $twig->render('pisosError.html', array('error'=>$error));
		}

		break;

	case 'borrar':
		$id=$_GET['id'];

		$consulta=$pisos->borrar($id);

		//Consulta ESTRICTA!!!!!!
		if($consulta===true){
			header('Location:index.php?contr=pisos.php&accion=listado');
		}else{
			$error=$consulta;
			echo $twig->render('pisosError.html', array('error'=>$error));
		}
		break;

	case 'modificar':
		$id=$_GET['id'];
		$e=$pisos->detalle($id);
		echo $twig->render('pisosModificar.html', array('e'=>$e));
		break;

	case 'modificacion':

		$idPiso=$_POST['idPiso'];
		$direccionPiso=$_POST['direccionPiso'];
		$caracteristicasPiso=$_POST['caracteristicasPiso'];
		$precioPiso=$_POST['precioPiso'];
		$ciudadPiso=$_POST['ciudadPiso'];

		if(is_uploaded_file($_FILES['imagenPiso']['tmp_name'])){
			$imagenPiso=time().$_FILES['imagenPiso']['name'];
			move_uploaded_file($_FILES['imagenPiso']['tmp_name'], 'imagenes/'.$imagenPiso);
		}else{
			$imagenPiso=$_POST['imagenPisoAnt'];
		}

		$consulta=$pisos->modificar($idPiso, $direccionPiso, $caracteristicasPiso, $precioPiso, $ciudadPiso, $imagenPiso);

		//Consulta ESTRICTA!!!!!!
		if($consulta===true){
			header('Location:index.php?contr=pisos.php&accion=listado');
		}else{
			$error=$consulta;
			echo $twig->render('pisosError.html', array('error'=>$error));
		}

		break;	

	case 'rss':
		$elementos=$pisos->listado(10,0);
		require('views/pisosRssView.php');
		break;

	case 'rssexterno':
		//Me creo un array vacio
		$elementos=[];
		//Por cada fuente RSS, creo un objeto de la clase rssModel
		$rss=new rssModel();
		//AITOR:
		$rss->add('http://192.168.1.54/aitor/20170621/pisosphp/index.php?controller=pisosController.php&accion=rss');

		//DAVID:
		$rss->add('http://192.168.1.58/datw/David/pisosphp/index.php?controller=pisosController.php&accion=rss');

		//FRANCISCO
		$rss->add('http://192.168.1.39/datw/David/pisosphp/index.php?controller=pisosController.php&accion=rss');

		//FABRICCIO
		$rss->add('http://192.168.1.47/Fabphp/DAVID/05_php/miercoles-21-junio-17/pisosphp/index.php?controller=pisosController.php&accion=rss');

		//JOSE ANTONIO
		$rss->add('http://192.168.1.59/pisosphp/index.php?controller=pisosController.php&accion=rss');

		//Extraigo los elementos de todas las fuentes
		$elementos=$rss->elementos();

		//Retornamos la vista
		require('views/pisosRssExternoView.php');
		break;

	case 'listado':
	default:
		//Estraigo el listado de pisos
		if(isset($_GET['numpag'])){
			$numpag=$_GET['numpag'];
		}else{
			$numpag=0;
		}
		$numregistrosporpagina=2;
		$numPaginas=$pisos->numPaginas($numregistrosporpagina);
		$elementos=$pisos->listado($numregistrosporpagina,$numpag);
		
		echo $twig->render('pisosListado.html', array('elementos'=>$elementos, 'numregistrosporpagina'=>$numregistrosporpagina, 'numPaginas'=>$numPaginas, 'numpag'=>($numpag)));

		break;
}
?>