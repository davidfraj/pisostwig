<?php 

// Fichero  models/class.noticiasModel.php

require('models/class.noticiaModel.php');

class NoticiasModel{

	private $elementos;
	private $conexion;

	public function __construct(){
		$this->elementos=[];
		$this->conexion=Conexion::$conexion;
	}

	public function listado($numregporpagina, $numpag=0){

		$inicio=$numregporpagina * $numpag;
		$sql="SELECT * FROM noticias LIMIT $inicio,$numregporpagina";
		$consulta=$this->conexion->query($sql);
		while($fila=$consulta->fetch_array()){
			$this->elementos[]=new NoticiaModel($fila);
		}
		return $this->elementos;
	}


	public function numPaginas($numregporpagina=2){
		$sql="SELECT * FROM noticias";
		$consulta=$this->conexion->query($sql);
		return ceil($consulta->num_rows/$numregporpagina);
	}


	public function detalle($id){
		$sql="SELECT * FROM noticias WHERE idNot=$id";
		$consulta=$this->conexion->query($sql);
		$fila=$consulta->fetch_array();
		return new NoticiaModel($fila);
	}


	public function insertar($tituloNot, $contenidoNot, $autorNot, $fechaNot){

		$sql="INSERT INTO noticias(tituloNot, contenidoNot, autorNot, fechaNot)VALUES('$tituloNot', '$contenidoNot', '$autorNot', '$fechaNot')";

		if($this->conexion->query($sql)==true){
			return true;
		}else{
			return $this->conexion->error;
		}

	}

	public function modificar($idNot, $tituloNot, $contenidoNot, $autorNot, $fechaNot){

		$sql="UPDATE noticias SET idNot='$idNot',tituloNot='$tituloNot', contenidoNot='$contenidoNot', autorNot='$autorNot' WHERE idNot='$idNot'";

		if($this->conexion->query($sql)==true){
			return true;
		}else{
			return $this->conexion->error;
		}

	}


		public function borrar($id){
		$sql="DELETE FROM noticias WHERE idNot=$id";
		if($this->conexion->query($sql)==true){
			return true;
		}else{
			return $this->conexion->error;
		}
	}



}




 ?>