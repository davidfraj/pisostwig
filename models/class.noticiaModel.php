<?php 

// Fichero  models/class.noticiaModel.php

class NoticiaModel{
	private $idNot;
	private $tituloNot;
	private $contenidoNot;
	private $autorNot;
	private $fechaNot;

	public function __construct($fila){
		$this->idNot=$fila['idNot'];
		$this->tituloNot=$fila['tituloNot'];
		$this->contenidoNot=$fila['contenidoNot'];
		$this->autorNot=$fila['autorNot'];
		$this->fechaNot=$fila['fechaNot'];
	}

    /**
     * Gets the value of idNot.
     *
     * @return mixed
     */
    public function getIdNot()
    {
        return $this->idNot;
    }

    /**
     * Gets the value of tituloNot.
     *
     * @return mixed
     */
    public function getTituloNot()
    {
        return $this->tituloNot;
    }

    /**
     * Gets the value of contenidoNot.
     *
     * @return mixed
     */
    public function getContenidoNot()
    {
        return $this->contenidoNot;
    }

    /**
     * Gets the value of autorNot.
     *
     * @return mixed
     */
    public function getAutorNot()
    {
        return $this->autorNot;
    }

    /**
     * Gets the value of fechaNot.
     *
     * @return mixed
     */
    public function getFechaNot()
    {
        return $this->fechaNot;
    }
}

 ?>