<?php 
// llamamos al cargados automatico de composer, para incluir
// todas las librerias
require('vendor/autoload.php');
require('classes/class.conexion.php');

//Creamos el objeto $twig de la clase Twig_Environment
$loader = new Twig_Loader_Filesystem('views/');
$twig = new Twig_Environment($loader);

//En esta parte del codigo, elegiremos que controlador queremos cargar
if(isset($_GET['contr'])){
	$contr=$_GET['contr'];
}else{
	$contr='pisos.php';
}

//Aqui llamariamos a las variables de configuracion...
$menuTitulos=array('Seccion de Pisos', 'Seccion de Noticias', 'Contacto');
$menuEnlaces=array('pisos.php', 'noticias.php', 'contacto.php');

//Una vez terminado la parte de variables fijas de mi web
//Llamo a las extensiones/funciones de twig
require('extensiones.php');


//Lo ultimo que se hace es INCLUIR EL CONTROLADOR
require('controllers/'.$contr);

?>